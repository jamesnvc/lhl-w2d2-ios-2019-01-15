//
//  ViewController.m
//  W2D2Demo
//
//  Created by James Cash on 15-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *theLabel;
@property (weak, nonatomic) UIView *theView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.theLabel.text = @"Hello Lighthouse";

    UIView *newView = [[UIView alloc] initWithFrame:CGRectMake(100, 350, 250, 250)];
    newView.backgroundColor = [UIColor blueColor];
    newView.clipsToBounds = YES; // makes overflowing children not visible
    [self.view addSubview:newView];
    self.theView = newView;

    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(50, 50, 150, 44)];
    [btn setTitle:@"Press Me" forState:UIControlStateNormal];
    [newView addSubview:btn];
//    [self.view insertSubview:btn belowSubview:newView];
    [btn addTarget:self action:@selector(changeColour:) forControlEvents:UIControlEventTouchUpInside];

    UIView *otherView = [[UIView alloc] initWithFrame:CGRectMake(60, 100, 100, 100)];
    otherView.backgroundColor = [UIColor purpleColor];
    [newView addSubview:otherView];
}

- (void)changeColour:(id)sender {
    NSArray<UIColor*>* colours = @[[UIColor redColor], [UIColor blueColor], [UIColor greenColor]];
    UIColor *colour = colours[arc4random_uniform(colours.count)];
    [UIView animateWithDuration:2.5 animations:^{
        self.theView.backgroundColor = colour;
    }];
}

- (IBAction)moveBounds:(UIButton *)sender {
    /* Super-annoying ugly way
    CGPoint newOrigin = CGPointMake(50, 60); // TODO make 60 be 10 + the current y origin
    CGRect newRect = { .origin = newOrigin, .size = self.theView.bounds.size };
     CGRect newRect;
     newRect.origin = newOrigin;
     newRect.size = self.theView.bounds.size;
    self.theView.bounds = newRect;
    */
    // want:
//    self.theView.bounds.origin.y += 10;
// using helper function:
    [sender setTitle:@"Moving..." forState:UIControlStateNormal];
    [UIView animateWithDuration:2 animations:^{
        self.theView.bounds = CGRectOffset(self.theView.bounds, 0, 10);
    } completion:^(BOOL finished) {
        NSLog(@"Done! %d", finished);
        [sender setTitle:@"Button" forState:UIControlStateNormal];
    }];
}

@end
