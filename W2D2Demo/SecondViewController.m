//
//  SecondViewController.m
//  W2D2Demo
//
//  Created by James Cash on 15-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonTopConstraint;

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UIView *view1 = [[UIView alloc] init];
    view1.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:view1];
    view1.backgroundColor = [UIColor cyanColor];
    /*
    NSLayoutConstraint* constraint1 =
    [NSLayoutConstraint constraintWithItem:view1
                                 attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:0.5
                                  constant:0];
    constraint1.active = YES;
     */
    [NSLayoutConstraint activateConstraints:
     @[
       [view1.widthAnchor constraintEqualToConstant:50],
       [view1.heightAnchor constraintEqualToAnchor:self.view.heightAnchor multiplier:0.5],
       [view1.topAnchor constraintEqualToAnchor:self.view.topAnchor],
       [view1.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor constant:-20]
       ]];
}

- (IBAction)moveSquare:(id)sender {
    [UIView animateWithDuration:2.5 animations:^{
        self.buttonTopConstraint.constant += 20;
        [self.view layoutSubviews];
    }];
}

@end
