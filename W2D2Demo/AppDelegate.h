//
//  AppDelegate.h
//  W2D2Demo
//
//  Created by James Cash on 15-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

